package com.manu.moviestaskproject.data.model

import com.google.gson.annotations.SerializedName


data class ResultsResponse<T>(
    @SerializedName("page")
    val page: Int,
    @SerializedName("total_results")
    val total_results: String? = null,
    @SerializedName("total_pages")
    val total_pages: String? = null,
    @SerializedName("results")
    val results: List<T>



)