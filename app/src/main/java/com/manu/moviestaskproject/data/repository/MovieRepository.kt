package com.manu.moviestaskproject.data.repository


import androidx.paging.*
import com.manu.moviestaskproject.data.local.MovieDao
import com.manu.moviestaskproject.data.local.MovieLocalDataSource
import com.manu.moviestaskproject.data.remote.ApiService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MovieRepository @Inject constructor(
    private val apiService: ApiService,
    private val movieDao: MovieDao,
){

    fun getMoviesResults(query: String, connectionAvailable: Boolean) =

            Pager(
                config = PagingConfig(
                    pageSize = 20,
                    maxSize = 100,
                    enablePlaceholders = false
                ),
                pagingSourceFactory = {
                    MovieLocalDataSource(apiService, movieDao, query)

                }
            ).liveData


}