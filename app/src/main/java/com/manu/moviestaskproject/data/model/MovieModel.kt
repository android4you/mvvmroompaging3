package com.manu.moviestaskproject.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "movies")
data class MovieModel(
    @PrimaryKey
    var id: Int,

    var vote_count: Int ,

    var isVideo: Boolean,

    var vote_average: Double,

    var title: String?,

    var popularity: Double,

    var poster_path: String?,

    var original_language: String?,

    var original_title: String?,

    var backdrop_path: String?,

    var isAdult: Boolean,

    var overview: String?,

    var release_date: String?,

    var type: Int?


)
