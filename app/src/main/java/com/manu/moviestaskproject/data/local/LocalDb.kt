package com.manu.moviestaskproject.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.manu.moviestaskproject.data.model.MovieModel




@Database(entities = arrayOf(MovieModel::class), version = 1, exportSchema = false)
abstract class LocalDb : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}