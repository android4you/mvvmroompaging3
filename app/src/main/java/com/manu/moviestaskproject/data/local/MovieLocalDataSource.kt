package com.manu.moviestaskproject.data.local

import androidx.paging.*
import com.manu.moviestaskproject.data.model.MovieModel
import com.manu.moviestaskproject.data.remote.ApiService
import retrofit2.HttpException
import java.io.IOException
private const val UNSPLASH_STARTING_PAGE_INDEX = 1

class MovieLocalDataSource  constructor(private val service: ApiService,
                                         private val dao: MovieDao,
                                         private val query: String)
    : PagingSource<Int, MovieModel>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieModel> {
        val position = params.key ?: UNSPLASH_STARTING_PAGE_INDEX

        return try {
            val type = when (query) {
                "popular" -> 1
                "upcoming" -> 2
                "top_rated" -> 3
                "now_playing" -> 4
                else -> 0
            }
            val movies = dao.fetchAllData(type)

            LoadResult.Page(
                data = movies,
                prevKey = if (position == UNSPLASH_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (movies.isEmpty()) null else position + 1
            )



        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

}