package com.manu.moviestaskproject.data.remote

import com.manu.moviestaskproject.data.model.MovieModel
import com.manu.moviestaskproject.data.model.ResultsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/{type}?")
    suspend fun getPopularMovies(@Path("type") type:String, @Query("api_key") api_key: String, @Query("page") page : Int): Response<ResultsResponse<MovieModel>>



}