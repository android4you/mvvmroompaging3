package com.manu.moviestaskproject.data.repository


import android.util.Log
import androidx.paging.PagingSource
import com.manu.moviestaskproject.BuildConfig
import com.manu.moviestaskproject.data.local.MovieDao
import com.manu.moviestaskproject.data.model.MovieModel
import com.manu.moviestaskproject.data.remote.ApiService
import com.manu.moviestaskproject.system.MyApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

private const val UNSPLASH_STARTING_PAGE_INDEX = 1

class MovieRemoteDataSource constructor(
    private val service: ApiService,
    private val dao: MovieDao,
    private val query: String
) : PagingSource<Int, MovieModel>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieModel> {
        val position = params.key ?: UNSPLASH_STARTING_PAGE_INDEX

        return try {
            val response = service.getPopularMovies(query, BuildConfig.API_KEY, position)

            val movies = response.body()?.results
            CoroutineScope(Dispatchers.IO).launch {
                val type = when (query) {
                    "popular" -> 1
                    "upcoming" -> 2
                    "top_rated" -> 3
                    "now_playing" -> 4
                    else -> 0
                }
                movies?.map {
                    it.type = type
                }
                dao.insertAll(movies!!)
                Log.e("size--> ", "" + dao.getSize())
            }

            LoadResult.Page(
                data = movies!!,
                prevKey = if (position == UNSPLASH_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (movies.isEmpty()) null else position + 1
            )


        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

}