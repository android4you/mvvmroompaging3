package com.manu.moviestaskproject.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.manu.moviestaskproject.data.model.MovieModel

@Dao
interface MovieDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<MovieModel>)

    @Query("SELECT * FROM movies WHERE type= :type")
   suspend fun fetchAllData(type: Int): List<MovieModel>

    @Query("SELECT count(*) from movies")
    fun getSize():Int



}