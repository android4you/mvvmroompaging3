package com.manu.moviestaskproject.system

import android.app.Application
import com.manu.moviestaskproject.data.local.LocalDb
import com.manu.moviestaskproject.di.component.ApplicationComponent
import com.manu.moviestaskproject.di.component.DaggerApplicationComponent

class MyApp: Application() {



    val  applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.factory().create(this.applicationContext)
    }



}