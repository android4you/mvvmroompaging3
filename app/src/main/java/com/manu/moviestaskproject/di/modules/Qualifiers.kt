package com.manu.moviestaskproject.di.modules

import javax.inject.Qualifier


@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class CoroutineScropeIO
