package com.manu.moviestaskproject.di.modules


import android.content.Context
import androidx.room.Room
import com.manu.moviestaskproject.data.local.LocalDb
import com.manu.moviestaskproject.data.local.MovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideLocalDb(applicationContext: Context): LocalDb {
        return Room.databaseBuilder(
            applicationContext,
            LocalDb::class.java, "movie_db"
        ).fallbackToDestructiveMigration().build()

    }

    @Provides
    @Singleton
    fun providesMovieDao(localdb: LocalDb): MovieDao {
        return localdb.movieDao()

    }


}











