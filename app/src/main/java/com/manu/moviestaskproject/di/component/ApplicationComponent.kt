package com.manu.moviestaskproject.di.component

import android.app.Application
import android.content.Context
import com.ia.insuranceauthority.di.module.ViewModelModule
import com.manu.moviestaskproject.di.modules.DbModule
import com.manu.moviestaskproject.di.modules.NetworkModule
import com.manu.moviestaskproject.di.modules.RepositoryModule
import com.manu.moviestaskproject.ui.home.HomeFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ViewModelModule::class, RepositoryModule::class, NetworkModule::class, DbModule::class])
interface ApplicationComponent {


    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): ApplicationComponent
    }
    fun inject(fragment: HomeFragment)

}