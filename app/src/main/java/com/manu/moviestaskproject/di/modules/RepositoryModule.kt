package com.manu.moviestaskproject.di.modules

import com.manu.moviestaskproject.data.local.MovieDao
import com.manu.moviestaskproject.data.remote.ApiService
import com.manu.moviestaskproject.data.repository.MovieRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton




@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideHomeRepository(
        apiService: ApiService,
         dao: MovieDao

    ): MovieRepository {
        return MovieRepository(apiService, dao)
    }
}