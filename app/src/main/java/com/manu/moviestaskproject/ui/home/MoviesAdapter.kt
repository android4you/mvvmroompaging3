package com.manu.moviestaskproject.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.manu.moviestaskproject.BuildConfig
import com.manu.moviestaskproject.R
import com.manu.moviestaskproject.data.model.MovieModel


class MoviesAdapter :
    PagingDataAdapter<MovieModel, MoviesAdapter.MovieViewHolder>(PHOTO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.movies_row, parent, false)
        return MovieViewHolder(v)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie: MovieModel? = getItem(position)
        if (movie != null) {
            holder.bindTo(movie)
        }
    }

    class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleTv: TextView
        var bannerIV: ImageView

        fun bindTo(movie: MovieModel) {
            titleTv.setText(movie.title)
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            Glide.with(itemView)  //2
                .load(BuildConfig.IMAGE_URL_SMALL + movie.poster_path) //3
                .apply(requestOptions)
                // .placeholder(R.drawable.ic_image_place_holder) //5
                //  .error(R.drawable.ic_broken_image) //6
                //  .fallback(R.drawable.ic_no_image) //7
                .into(bannerIV) //8

        }

        init {
            titleTv = itemView.findViewById<View>(R.id.title) as TextView
            bannerIV = itemView.findViewById<View>(R.id.bannerimg) as ImageView
        }
    }

    companion object {
        private val PHOTO_COMPARATOR = object : DiffUtil.ItemCallback<MovieModel>() {
            override fun areItemsTheSame(oldItem: MovieModel, newItem: MovieModel) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: MovieModel, newItem: MovieModel) =
                oldItem == newItem
        }
    }
}