package com.manu.moviestaskproject.ui.home

import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.manu.moviestaskproject.data.repository.MovieRepository
import com.manu.moviestaskproject.di.modules.CoroutineScropeIO
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject


class HomeViewModel @Inject constructor(
    val movieRepository: MovieRepository,
    @CoroutineScropeIO private val ioCoroutineScope: CoroutineScope
) : ViewModel() {


    val movies =
        movieRepository.getMoviesResults("popular", false).cachedIn(viewModelScope)

    val upcoming =
        movieRepository.getMoviesResults("upcoming", false).cachedIn(viewModelScope)

    val nowplaying =
        movieRepository.getMoviesResults("now_playing", false).cachedIn(viewModelScope)

    val topRated =
        movieRepository.getMoviesResults("top_rated", false).cachedIn(viewModelScope)
}