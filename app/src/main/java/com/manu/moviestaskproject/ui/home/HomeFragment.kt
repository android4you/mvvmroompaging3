package com.manu.moviestaskproject.ui.home

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.ia.insuranceauthority.di.module.ViewModelFactory
import com.manu.moviestaskproject.R
import com.manu.moviestaskproject.di.component.DaggerApplicationComponent
import com.manu.moviestaskproject.system.MyApp
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment: Fragment(R.layout.fragment_home) {

    private lateinit var viewModel: HomeViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var popularMoviesAdapter: MoviesAdapter? =null
    private var upcomingMoviesAdapter: MoviesAdapter? =null
    private var topratedMoviesAdapter: MoviesAdapter? =null
    private var nowplayingMoviesAdapter: MoviesAdapter? =null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val component = (requireActivity().application as MyApp).applicationComponent
        component.inject(this)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[HomeViewModel::class.java]

        popularMoviesAdapter = MoviesAdapter()
        popular_movies_list.adapter = popularMoviesAdapter
        viewModel.movies.observe(viewLifecycleOwner) {
            popularMoviesAdapter!!.submitData(viewLifecycleOwner.lifecycle, it)
        }

        upcomingMoviesAdapter = MoviesAdapter()
        upcoming_movies_list.adapter = upcomingMoviesAdapter
        viewModel.upcoming.observe(viewLifecycleOwner) {
            upcomingMoviesAdapter!!.submitData(viewLifecycleOwner.lifecycle, it)
        }

        topratedMoviesAdapter = MoviesAdapter()
        top_ratedlist.adapter = topratedMoviesAdapter
        viewModel.topRated.observe(viewLifecycleOwner) {
            topratedMoviesAdapter!!.submitData(viewLifecycleOwner.lifecycle, it)
        }

        nowplayingMoviesAdapter = MoviesAdapter()
        nowplayinglist.adapter = nowplayingMoviesAdapter
        viewModel.nowplaying.observe(viewLifecycleOwner) {
            nowplayingMoviesAdapter!!.submitData(viewLifecycleOwner.lifecycle, it)
        }

    }

}